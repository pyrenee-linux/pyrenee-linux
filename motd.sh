#! /bin/bash

echo "Welcome to Pyrenee Linux!"
echo ""
echo "This isn't your typical Linux distribution, for example 'sudo' isn't  installed but instead you can use 'doas' to elevate your priviliges."
echo "For your convenience we've added an system-wide alias to replace 'sudo' with 'doas' which would alliviate the switch-over, but please be aware this *could* lead to issues."
echo ""
echo "Please report any issues to our GitLab, found at https://gitlab.com/pyrenee-linux/pyrenee-linux/-/issues"
